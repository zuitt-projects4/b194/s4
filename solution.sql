
--a.
SELECT (name) FROM artists WHERE name LIKE "%d%";

--b. 
SELECT * FROM songs WHERE length < 230;

--C.
SELECT albums.album_title, songs.song_name, songs.length
 FROM songs JOIN albums ON albums.id = songs.album_id; 

 --d.
 SELECT albums.album_title, artists.name FROM albums 
 	JOIN artists ON artists.id = albums.artists_id
 	WHERE album_title LIKE "%a%";

 --e.
 SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;


--f.
SELECT albums.album_title, songs.song_name FROM songs 
	JOIN albums ON albums.id = songs.album_id 
 	ORDER BY albums.album_title DESC;

SELECT albums.album_title, songs.song_name FROM songs 
	JOIN albums ON albums.id = songs.album_id 
 	ORDER BY songs.song_name ASC;